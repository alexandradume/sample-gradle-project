package com.example;

public class Max implements Operation {
    @Override
    public int calculate(Integer a, Integer b) {
        if(a > b)
            return a;
        else
            return b;
    }
}
