package com.example;

public class Sqrt implements Operation {
    @Override
    public int calculate(Integer a, Integer b) {
        if(a > b)
            return a;
        else
            return b;
    }
}
