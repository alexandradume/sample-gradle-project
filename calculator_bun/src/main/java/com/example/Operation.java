package com.example;

public interface Operation {
    public int calculate(Integer a, Integer b);

    //public double calculate(Integer a);
}
