package com.example;

public class Division implements Operation{
    @Override
    public int calculate(Integer a, Integer b) {
        return a/b;
    }
}
