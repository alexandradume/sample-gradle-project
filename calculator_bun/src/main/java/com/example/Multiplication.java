package com.example;

public class Multiplication implements Operation{
    @Override
    public int calculate(Integer a, Integer b) {
        return a*b;
    }
}
