package com.example;

public class Min implements Operation{
    @Override
    public int calculate(Integer a, Integer b) {
        if(a > b)
            return b;
        else
            return a;
    }
}
