package com.example;

public class Addition implements Operation{
    @Override
    public int calculate(Integer a, Integer b) {
        return a+b;
    }
}
