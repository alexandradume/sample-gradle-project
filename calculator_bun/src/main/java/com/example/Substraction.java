package com.example;

public class Substraction implements Operation{
    @Override
    public int calculate(Integer a, Integer b) {
        return a-b;
    }
}
