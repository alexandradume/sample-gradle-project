package com.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinTest {
    @Test
    void calculate() {
        Min min = new Min();
        int result = min.calculate(8,2);
        assertEquals(2,result);
        int result2 = min.calculate(9,8);
        assertEquals(8,result2);
    }

}