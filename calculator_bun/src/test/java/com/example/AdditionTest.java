package com.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AdditionTest {

    @Test
    void calculate() {
        Addition addition = new Addition();
        int result = addition.calculate(8,2);
        assertEquals(10,result);
    }
}