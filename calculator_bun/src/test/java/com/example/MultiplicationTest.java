package com.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultiplicationTest {
    @Test
    void calculate() {
        Multiplication multiplication = new Multiplication();
        int result = multiplication.calculate(8,2);
        assertEquals(16,result);
    }

}