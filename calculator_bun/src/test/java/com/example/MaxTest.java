package com.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxTest {
    @Test
    void calculate() {
        Min min = new Min();
        int result = min.calculate(8,2);
        assertEquals(8,result);
        int result2 = min.calculate(9,8);
        assertEquals(9,result2);
    }
}