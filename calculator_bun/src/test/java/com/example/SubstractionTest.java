package com.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubstractionTest {
    @Test
    void calculate() {
        Substraction substraction = new Substraction();
        int result = substraction.calculate(8,2);
        assertEquals(6,result);
    }

}